import requests
from bs4 import BeautifulSoup
import urllib.request
from prettytable import PrettyTable
import argparse
import string as st


headers = {"User-Agent": 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36'}

pccDoc = "PCComponentes_Links"
amzDoc = "Amazon_Links"
marktDoc = "MediaMarkt_Links"
CorteIngDoc = "CorteIngles_Links"
gameDoc = "Game_Links"
fnacDoc = "FNAC_Links"


parser = argparse.ArgumentParser()
parser.add_argument("-order", "-o", type=str, metavar = '',required = False, help="Order products by [price]")
parser.add_argument("-filter", "-f", type=str, metavar = '',required= False, help="Filter by <string>", default = "")

requiredNamed = parser.add_argument_group('required arguments')
requiredNamed.add_argument("-shop", "-s", type=str, metavar = '', required = True, help="Shop to be scraped [pcc, markt, ing, amz, game, fnac, ALL]")

args = parser.parse_args()


def stringToInt(priceString):
    priceStr = ""
    priceInt = 0
    contador = 0
    i=0

    numbers = ['0','1','2','3','4','5','6','7','8','9']

    while i<3:
        char = priceString[contador]

        if(char in numbers):
            priceStr += char
            i+=1
        contador += 1

    priceInt = int(priceStr)

    return priceInt

def removeBlanks(word):

    firstFound = False
    contador = 0

    newWord = ""
    firstPos = 0

    while firstFound == False:
        char = word[contador]
        if char.isalpha():
            newWord+=char
            firstPos = contador
        else:
            contador += 1


    for i in range(contador, len(word)):
        char = word[i]
        if(char.isalpha()):
            newWord += char

    return newWord

class Product:
    def __init__(self, name, price, link):
        self.name = name
        self.price = price
        self.link = link

    def __str__(self):
        return (self.name + ": " + str(self.price))

class Shop:
    name = ""
    products = []

    def __init__(self, shopName = ""):
        self.name = shopName
        self.products = []

    def print(self):
        t = PrettyTable(["nº", self.name, 'Price', 'Link'])
        contador = 1
        for i in self.products:
            t.add_row([contador, i.name, i.price, i.link])
            contador+=1
        print(t)

    def getSize(self):
        return len(self.products)

    def getProduct(self, i):
        return self.products[i]

    def add(self, product):
        self.products.append(product)

    def order(self):
        print("Ordering...")
        self.products.sort(key=lambda x:x.price)

    def filter(self, name):
        print("Filtering by key word = \"" + name + "\"... ")
        filterProducts = []

        for product in self.products:
            index = product.name.find(name)
            if index != -1:
                filterProducts.append(product)

        self.products = filterProducts


def scrapeMarkt():
    print("Scraping MediaMarkt...")
    shop = Shop()

    fstream = open(marktDoc, 'r')
    nLines = fstream.readlines()

    for line in nLines:
        pageName = line[:-1]
        page = requests.get(pageName)
        soup = BeautifulSoup(page.content, 'html.parser')

        title = soup.find(itemprop="name").get_text().strip()
        price = soup.find(class_="product-crossed-price").get_text().strip()
        priceInt = stringToInt(price)

        product = Product(title, priceInt, pageName)
        shop.add(product)

    fstream.close()

    return shop

def scrapeCorteIng():
    print("Scraping Corte Inglés...")
    shop = Shop()

    fstream = open(CorteIngDoc, 'r')
    nLines = fstream.readlines()

    for line in nLines:
        pageName = line[:-1]
        page = requests.get(pageName)
        soup = BeautifulSoup(page.content, 'html.parser')


        title = soup.find(class_="truncate wrap").get_text().strip()
        price = soup.find(itemprop="price").get_text().strip()
        priceInt = stringToInt(price)

        product = Product(title, priceInt, pageName)
        shop.add(product)


    fstream.close()

    return shop

def scrapePCComp():
    print("Scraping PC Componentes...")
    shop = Shop()

    fstream = open(pccDoc, 'r')
    nLines = fstream.readlines()

    for line in nLines:
        pageName = line[:-1]
        page = requests.get(pageName)
        soup = BeautifulSoup(page.content, 'html.parser')

        title = soup.find(class_ = "h4").get_text().strip()
        price = soup.find(id="precio-main").get_text()
        priceInt = stringToInt(price)

        product = Product(title, priceInt, pageName)
        shop.add(product)

    fstream.close()

    return shop

def scrapeAmaz():
    print("Scraping Amazon...")

    shop = Shop()

    fstream = open(amzDoc, 'r')
    nLines = fstream.readlines()

    for line in nLines:
        pageName = line[:-1]
        page = requests.get(pageName,  headers=headers)
        soup = BeautifulSoup(page.content, 'lxml')

        title = soup.find(id="productTitle").get_text().strip()
        titleShort = (title[:50] + '..') if len(title) > 75 else title
        price = soup.find(id = "priceblock_ourprice")

        if(price != None):
            priceText = price.get_text().strip()
            priceInt = stringToInt(priceText)
            product = Product(titleShort, priceInt, pageName)
            shop.add(product)
        else:
            print("Error procesando el link: " + pageName)


    fstream.close()

    return shop

def scrapeGame():
    print("Scraping GAME...")
    shop = Shop()

    fstream = open(gameDoc, 'r')
    nLines = fstream.readlines()

    for line in nLines:
        pageName = line[:-1]
        page = requests.get(pageName, headers=headers)
        soup = BeautifulSoup(page.content, 'html.parser')

        title = soup.find(class_ = "product-title")
        if(title != None):
            titleString= title.get_text().strip()
            price = soup.find(class_ = "buy--price")
            if(price != None):
                priceString = price.get_text()
                priceInt = stringToInt(priceString)
                product = Product(titleString, priceInt, pageName)
                shop.add(product)
        else:
            print("Error procesando el link: " + pageName)

    fstream.close()

    return shop

def scrapeFnac():
    print("Scraping FNAC...")
    shop= Shop()

    fstream = open(fnacDoc, 'r')
    nLines = fstream.readlines()

    for line in nLines:
        pageName = line[:-1]
        page = requests.get(pageName)
        soup = BeautifulSoup(page.content, 'html.parser')

        title = soup.find(class_ = "FAstrate-subtitle").get_text().strip()
        price = soup.find(class_ = "f-priceBox-price f-priceBox-price--reco checked").get_text()

        priceInt = stringToInt(price)

        product = Product(title, priceInt, pageName)
        shop.add(product)

    fstream.close()

    return shop

def scrapeAll():
    markt = scrapeMarkt()
    ing = scrapeCorteIng()
    pcc = scrapePCComp()
    game = scrapeGame()
    fnac = scrapeFnac()
    amazon = scrapeAmaz()

    total = Shop("Total")

    for i in range(0, markt.getSize()):
        total.add(markt.getProduct(i))
    for i in range(0, ing.getSize()):
        total.add(ing.getProduct(i))
    for i in range(0, pcc.getSize()):
        total.add(pcc.getProduct(i))
    for i in range(0, game.getSize()):
        total.add(game.getProduct(i))
    for i in range(0, fnac.getSize()):
        total.add(fnac.getProduct(i))
    for i in range(0, amazon.getSize()):
        total.add(amazon.getProduct(i))

    return total

def main():


    if args.shop == "pcc" or args.shop == "pccomponentes":
        shop = scrapePCComp()
        shop.name = "PC Componentes"

    elif args.shop == "markt" or args.shop == "mediamarkt":
        shop = scrapeMarkt()
        shop.name = "MediaMarkt"

    elif args.shop == "ing" or args.shop == "corteingles":
        shop = scrapeCorteIng()
        shop.name = "Corte inglés"

    elif args.shop == "amz" or args.shop == "amazon":
        shop = scrapeAmaz()
        shop.name = "Amazon"

    elif args.shop == "game":
        shop = scrapeGame()
        shop.name = "GAME"

    elif args.shop == "fnac":
        shop = scrapeFnac()
        shop.name = "FNAC"

    elif args.shop == "ALL":
        shop = scrapeAll()
        shop.name = "ALL"

    if args.filter is not None:
        if args.filter != "":
            shop.filter(args.filter)

    if args.order is not None:
        if args.order == "price":
            shop.order()

    shop.print()

main()
