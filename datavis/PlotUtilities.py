import plotly.graph_objs as go

class PlotInfo:
    def __init__(self, title, colors, legend, xLabels, data, layout):
        self.title = title
        self.colors = colors
        self.legend = legend
        self.xLabels = xLabels
        self.data = data
        self.layout = layout

    # def title(self):
    #     return self.title

    # def colors(self):
    #     return self.colors

    # def legend(self):
    #     return self.legend

    # def xLabels(self):
    #     return self.xLabels

    # def data(self):
    #     return self.data

    # def layout(self):
    #     return self.layout


class Plot:
    def __init__(self, plotInfo):
        self.plotInfo = plotInfo

    def getModePlot(self, mode):
        if mode == "bar":
            return BarPlot(self.plotInfo)
        # elif mode == "pie":
        #     return PiePlot(self.data, self.title, self.colors, self.layout)
        # elif mode == "line":
        #     return LinePlot(self.data, self.title, self.colors, self.layout)

    def createData(self):
        raise NotImplementedError()

    def setTitle(self, title):
        self.title = title

    def setColors(self, colors):
        self.colors = colors

    def setLayout(self, layout):
        self.layout = layout

class BarPlot(Plot):
    def createData(self):
        bars = []
        contador = 0
        for data in self.plotInfo.data:
            bar = go.Bar(
                name = self.plotInfo.legend[contador],
                x = self.plotInfo.xLabels,
                y = data,
                marker=dict(color=self.plotInfo.colors[contador])
            )
            contador += 1
            bars.append(bar)

        
        return bars

class PiePlot(Plot):
    def createData(self):
        return [go.Pie(
            labels=self.data[0],
            values=self.data[1],
            marker=dict(colors=self.colors)
        )
        ]

class LinePlot(Plot):
    def createData(self):
        return [go.Scatter(
            x=self.data[0],
            y=self.data[1],
            marker=dict(color=self.colors)
        )
        ]

class FunctionPlot(Plot):
    def createData(self):
        return [go.Scatter(
            x=self.data[0],
            y=self.data[1],
            marker=dict(color=self.colors)
        )
        ]





