import sys
import plotly.graph_objs as go
import os

import input
import PlotUtilities

if len(sys.argv) == 3:
    archivo = sys.argv[1]
    mode = sys.argv[2]
else:
    print('ERROR: hello.py <archivo> <modo>', file=sys.stderr)
    sys.exit(1)

title = input.readTitle(archivo)
titlePlot = ""
for word in title:
    titlePlot += word

colors = input.readColors(archivo)
legend = input.readLegend(archivo)
xLabels = input.readXLabels(archivo)
data = input.readData(archivo)
layout = go.Layout(title_text = titlePlot)

print(legend)
plotInfo = PlotUtilities.PlotInfo(title, colors, legend, xLabels, data, layout)
plotData = PlotUtilities.Plot(plotInfo).getModePlot(mode).createData()

fig = go.Figure(data=plotData, layout=layout)
input.exportPNG(fig, titlePlot)