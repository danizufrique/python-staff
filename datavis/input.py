import os
import plotly.graph_objs as go

colors = {
  "blue": "#30C7FF",
  "red": "#FC451D",
  "yellow": "#FFEC4E",
  "magenta": "#BB0367",
  "green": "#2BE522",
  "black": "#000000",
  "white": "#FFFFFF",
  "purple": "#A70CCD",
  "orange": "#FFB129"
}

def readValue(key, file):
    with open(file,'r') as file: 
        res = []
        for line in file: 
            words = line.split() 
            if words[0] == key:
                for i in range(1, len(words)): 
                    res.append(words[i])
                return res
    return []

def readTitle(file):
    return readValue("title:", file)

def readColors(file):
    return readValue("colors:", file)        
    
def readLegend(file):
    return readValue("legend:", file)

def readXLabels(file):
    return readValue("xLabels:", file)

def readData(file):
    read = False
    with open(file,'r') as file: 
        data = []
        for line in file: 
            words = line.split() 
            
            if read:
                row = [] 
                for word in words: 
                    row.append(word)
                data.append(row)
            
            if words[0] == "data:":
                read = True
    return data

def exportPNG(figure, title):
    if not os.path.exists("images"):
        os.mkdir("images")

    figure.write_image("images/" + title + ".png")

